/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

final class Assets {
  public static final String PLAYER_1_RIGHT   = "assets/neal-bog-pirate/right/";
  public static final String PLAYER_2_RIGHT   = "assets/neal-bog-pirate-2/right/";
  public static final String PLAYER_3_RIGHT   = "assets/neal-bog-pirate-3/right/";
  public static final String PLAYER_4_RIGHT   = "assets/neal-bog-pirate-4/right/";
  public static final String PLAYER_1_LEFT    = "assets/neal-bog-pirate/left/";
  public static final String PLAYER_2_LEFT    = "assets/neal-bog-pirate-2/left/";
  public static final String PLAYER_3_LEFT    = "assets/neal-bog-pirate-3/left/";
  public static final String PLAYER_4_LEFT    = "assets/neal-bog-pirate-4/left/";
  public static final String PLAYER_1_FRONT   = "assets/neal-bog-pirate/front/0.png";
  public static final String PLAYER_2_FRONT   = "assets/neal-bog-pirate-2/front/0.png";
  public static final String PLAYER_3_FRONT   = "assets/neal-bog-pirate-3/front/0.png";
  public static final String PLAYER_4_FRONT   = "assets/neal-bog-pirate-4/front/0.png";
  
  public static final String BOARD_BACKGROUND    = "assets/board.png";
  public static final String GAMEOVER_BACKGROUND = "assets/gameover.png";
  public static final String SELECT_PLAYER_BACKGROUND = "assets/playerselect.png";
  
  public static final String SQUARE_IMG          = "assets/boxCrate.png";
  public static final String SQUARE_MASK         = "assets/boxItem_boxed.png";
  public static final String KEY_IMG             = "assets/key.png";
  public static final String TREASURE_CLOSE_IMG  = "assets/treasure_close.png";
  public static final String TREASURE_OPENED_IMG = "assets/treasure_open.png";
  public static final String TREASURE_EMPTY_IMG  = "assets/treasure_empty.png";

  public static final String PIRATE_FONT = "assets/fonts/GFSJackson.otf";
}
