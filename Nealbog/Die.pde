/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Die {
  private float x;
  private float y;
  private int value;
  private float rolledAt;
  private int timeShowed;
  private int size;
  private boolean locked;
  private boolean rolling;
  private float counter;
  private int nbRoll;

  public Die(float x, float y) {
    this.x = x;
    this.y = y;
    size = 55;
    value = 0;
    rolledAt = .0f;
    timeShowed = 1500;
    rolling = false;
    counter = .0f;
    nbRoll = 0;
  }
  
  public void update(float delta) {
    counter += delta;
  }
  
  public void draw() {
    if (!locked) {
      fill(255, 255, 255);
      rect(x, y, size, size, 7);
      fill(0, 0, 0);
      ellipseMode(CENTER);
      ellipse(x + size * .2f, y + size * .2f, 5, 5);
      ellipse(x + size * .8f, y + size * .2f, 5, 5);
      ellipse(x + size * .2f, y + size * .8f, 5, 5);
      ellipse(x + size * .8f, y + size * .8f, 5, 5); 
    }

    if (rolling) {
      if (counter > .1f) {
        value = (int) random(1, 7);
        nbRoll++;
        counter = .0f;
      }
      if (nbRoll > 5) {
        rolling = false;
        rolledAt = millis();
        counter = .0f;
      }
      this.drawDie();
    }
    
    if (rolledAt != .0f && 
          rolledAt + timeShowed > millis()) {
      this.drawDie();
    }
  }

  public boolean endOfRoll() {
    return nbRoll > 5;
  }
 
  public void roll() {
    if (!locked) {
      nbRoll = 0;
      rolledAt = .0f;
      rolling = true;
      locked = true;
      value = (int) random(1, 7);
    }
  }
  
  public int getValue() {
    return value;
  }
  
  public boolean isOver(PVector p) {
    if (p.x > x && p.x < x + size &&
        p.y > y && p.y < y + size) {
      return true;
    }
    return false;
  }
  
  public void unlock() {
    locked = false;
  }
  
  public boolean isLocked() {
    return locked;
  }

  void drawDie() {
    fill(255, 255, 255);
    rect(width / 2 - size, height / 2 - size, size * 2, size * 2);
    ellipseMode(CENTER);
    fill(0, 0, 0);
    if (value == 1 || value == 5 || value == 3) {
      ellipse(width / 2, height / 2, 10, 10);  
    }
    if (value == 2 || value == 6 || value == 3 || value == 4 || value == 5) {
      ellipse(width / 2 + size * .7f, height / 2 - size * .7f, 10, 10);
      ellipse(width / 2 - size * .7f, height / 2 + size * .7f, 10, 10);
    }
    if (value == 5 || value == 4 || value == 6) {
      ellipse(width / 2 + size * .7f, height / 2 + size * .7f, 10, 10);
      ellipse(width / 2 - size * .7f, height / 2 - size * .7f, 10, 10);
    } if (value == 6) {
      ellipse(width / 2 + size * .7f, height / 2, 10, 10);
      ellipse(width / 2 - size * .7f, height / 2, 10, 10);
    }
  }
}
