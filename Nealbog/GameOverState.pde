/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class GameOverState implements IState {
  private PImage background;
  private PImage player;
  private GameController gameController;
  
  private PVector newGameVector;
  private String newGame; 
  
  private PVector quitGameVector;
  private String quitGame;
  
  private boolean newGameClick;
  private boolean quitGameClick;
  
  private float counter;
  private boolean blink;
  private String playerWin;
  
  GameOverState(GameController gameController) {
    this.gameController = gameController;
  }
  
  void init() {
    this.background = loadImage(Assets.GAMEOVER_BACKGROUND);
    this.newGame = "New Game";
    this.quitGame = "Quit";
    //playerWin = "Joueur 1 remporte la partie!";
    this.player = loadImage(this.gameController.getCurrentPlayer().getFront());
    this.playerWin = gameController.getCurrentPlayer().name + " remporte la partie !";
    this.gameController.registerPlayerWin(this.gameController.getCurrentPlayer());
    
    this.newGameVector = new PVector(width * .5f, height * .7f);
    this.quitGameVector = new PVector(width * .5f, height * .8f);
    
    this.newGameClick = false;
    this.quitGameClick = false;
    this.blink = false;
    this.counter = .0f;
  }
  
  void handleInput(InputHandler inputHandler) {
    if (inputHandler.getTouches().length == 0) return;
    
    Touch touch = inputHandler.getTouches()[0];
    
    if (touch.x > this.newGameVector.x - width * .1f && 
          touch.x < this.newGameVector.x + width * .1f &&
          touch.y > this.newGameVector.y - height * .05f && 
          touch.y < this.newGameVector.y + height * .05f
        ) {
      this.newGameClick = true;
    }
    
    if (touch.x > this.quitGameVector.x - width * .1f && 
          touch.x < this.quitGameVector.x + width * .1f &&
          touch.y > this.quitGameVector.y - height * .05f && 
          touch.y < this.quitGameVector.y + height * .05f
        ) {
      this.quitGameClick = true;
    }
  }
  
  IState update(float delta) {
    this.counter += delta;
    if (this.counter >= 1f) {
      this.blink = !this.blink;
      this.counter = .0f;
    }
    
    if (this.newGameClick) {
      return new PlayerSelectState(this.gameController);
    }
    
    if (this.quitGameClick) {
      exit();
    }
    
    return this;   
  }
  
  void draw() {
    image(this.background, 0, 0, width, height);
    textAlign(CENTER);
    if (this.blink) {
      textSize(52);
      text(this.playerWin, width * .5f, height * .1f);
      image(this.player, width * .5f, height * .2f, this.player.width, this.player.height);
    }
    textSize(46);
    
    text(this.newGame, this.newGameVector.x, this.newGameVector.y);
    text(this.quitGame, this.quitGameVector.x, this.quitGameVector.y);
  }
  
}
