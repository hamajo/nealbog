/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class MiniGameTreasure extends MiniGame {
  private boolean win;
  private boolean lose;
  private boolean startGame;
  private boolean touchLock;

  private float timeLimit;
  private float gameOver;
  private float size;

  private TreasureChest[] chests;
  private PImage chestClosed;
  private PImage chestOpened;
  private PImage chestEmpty;
  private PImage background;

  private float countdown;


  public MiniGameTreasure() {
    this.win = false;
    this.lose = false;
    this.timeLimit = 5000f;
    this.startGame = false;
    this.size = 100;
    this.touchLock = false;
    this.countdown = .0f;
  }

  public void init() {
      this.win = false;
      this.lose = false;
      this.startGame = false;
      this.timeLimit = 5000f;

      TreasureChest[] c = {
        new TreasureChest(width * .15f, height * .05f), new TreasureChest(width * .35f, height * .05f),
        new TreasureChest(width * .55f, height * .05f), new TreasureChest(width * .75f, height * .05f),

        new TreasureChest(width * .15f, height * .25f), new TreasureChest(width * .35f, height * .25f),
        new TreasureChest(width * .55f, height * .25f), new TreasureChest(width * .75f, height * .25f),

        new TreasureChest(width * .15f, height * .45f), new TreasureChest(width * .35f, height * .45f),
        new TreasureChest(width * .55f, height * .45f), new TreasureChest(width * .75f, height * .45f),

        new TreasureChest(width * .15f, height * .65f), new TreasureChest(width * .35f, height * .65f),
        new TreasureChest(width * .55f, height * .65f), new TreasureChest(width * .75f, height * .65f),

        new TreasureChest(width * .15f, height * .85f), new TreasureChest(width * .35f, height * .85f),
        new TreasureChest(width * .55f, height * .85f), new TreasureChest(width * .75f, height * .85f),
      };

      chests = c;
      int n = (int) random(0, c.length);
      chests[n].winning = true;
      chestClosed = loadImage(Assets.TREASURE_CLOSE_IMG);
      chestOpened = loadImage(Assets.TREASURE_OPENED_IMG);
      chestEmpty = loadImage(Assets.TREASURE_EMPTY_IMG);
      background = loadImage(Assets.BOARD_BACKGROUND);
  }

  public void handleInput(InputHandler inputHandler) {
      if (inputHandler.getTouches().length == 0) {
        touchLock = false;
        return;
      }

      if (!startGame) {
        for (Touch t : inputHandler.getTouches()) {
          if (t.y > height / 4 && t.y < height * .75f) {
            gameOver = millis() + timeLimit;
            startGame = true;
            touchLock = true;
            return;
          }
        }
      } else if (!touchLock && !this.win && !this.lose) {
        for (Touch t : inputHandler.getTouches()) {
          for (TreasureChest c : chests) {
            if (t.y > c.y && t.y < c.y + size &&
                t.x > c.x && t.x < c.x + size) {
              if (c.winning) {
                win = true;
                c.opened = true;
                countdown = .0f;
              } else {
                c.opened = true;
              }
            }
          }
        }
      }
  }

  public IState update(float delta) {
    countdown += delta;
    if (millis() > gameOver && startGame && !this.lose) {
      this.lose = true;
      countdown = .0f;
    }

    if (this.win && countdown > 3f) {
      return win();
    }

    if (this.lose && countdown > 3f) {
      return lose();
    }

    return keepOn();
  }

  public void draw() {
    image(background, 0, 0, width, height);
    textAlign(CENTER);
    fill(255, 255, 255);
    if (!startGame) {
      textSize(28);
      text("Trouvez le vrai tresor dans le temps imparti !", width / 2, height / 4);

      textSize(42);
      text("START MINI GAME", width / 2, height / 2);
    } else if (!this.lose && !this.win) {
      textSize(72);
      int timeLeft = (int) ((gameOver - millis()) / 1000);
      text("" + timeLeft, width * .95f, height * .1f);
      for (TreasureChest t : chests) {
        if (t.opened) {
          if (t.winning) {
            image(chestOpened, t.x, t.y, size, size);
          } else {
            image(chestEmpty, t.x, t.y, size, size);
          }
        } else {
          image(chestClosed, t.x, t.y, size, size);
        }
      }
    }

    if (this.win || this.lose) {
      for (TreasureChest t : chests) {
        if (t.winning) {
          image(chestOpened, t.x, t.y, size, size);
        }
      }
    }

    if (this.win) {
      textSize(88);
      text("VICTOIRE !", width / 2, height / 2);
    } else if (this.lose) {
      textSize(88);
      text("DEFAITE !", width / 2, height / 2);
    }

    fill(0, 0, 0);
  }
}
