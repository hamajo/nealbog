/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Player {
  private final int numberOfTiles = 3;
  private String name;
  public int id;
  private int playerNumber;
  private int keys;
  
  private float x;
  private float y;
  
  private float counter;
  private int currentTile;
  private boolean isCurrentPlayer;
  
  private boolean goingLeft;
  private float size;
  private boolean offsetActive;
  
  private PImage img;
  
  Player(int id, String name) {
    this(id, name, 0);
  }
  
  Player(int id, String name, int playerNumber) {
    this.id = id;
    this.name = name;
    this.playerNumber = playerNumber;
    this.counter = .0f;
    this.isCurrentPlayer = false;
    this.currentTile = 1;
    this.goingLeft = true;
    this.size = 1;
    this.offsetActive = false;
    this.keys = 0;
  }
  
  void update(float delta) {
    if (this.isCurrentPlayer) {
      this.counter += delta;
      if (this.counter > .2f) {
        this.currentTile += 1;
        this.currentTile = this.currentTile % this.numberOfTiles;
        this.counter = .0f;
      }
    } else {
      this.currentTile = 0;
    }
  }
  
  void draw() {
    if (this.isCurrentPlayer) {
      this.img = loadImage(this.getTiles() + this.currentTile + ".png");
    } else {
      this.img = loadImage(this.getFront());
    }
    
    if (this.offsetActive) {
      switch(this.playerNumber) {
        case 1: {
          image(this.img, this.x - width * .02f, this.y, 
                this.img.width * this.size, this.img.height * this.size);
          break;
        }
        case 2: {
          image(this.img, this.x, this.y - height * .02f, 
                this.img.width * this.size, this.img.height * this.size);
          break;  
        }
        case 3: {
          image(this.img, this.x + width * .02f, this.y, 
                this.img.width * this.size, this.img.height * this.size);
          break;
        }
        case 4: {
          image(this.img, this.x, this.y + height * .02f, 
                this.img.width * this.size, this.img.height * this.size);
          break;
        }
        default: {
          image(this.img, this.x, this.y, 
                this.img.width * this.size, this.img.height * this.size);
          break;
        }
      } 
    } else {
      image(this.img, this.x, this.y, 
            this.img.width * this.size, this.img.height * this.size);
    }
  }
  
  private String getTiles() {
    switch(this.playerNumber) {
      case 1: {
        return this.goingLeft ? Assets.PLAYER_1_LEFT : Assets.PLAYER_1_RIGHT;
      }
      case 2: {
        return this.goingLeft ? Assets.PLAYER_2_LEFT : Assets.PLAYER_2_RIGHT;
      }
      case 3: {
        return this.goingLeft ? Assets.PLAYER_3_LEFT : Assets.PLAYER_3_RIGHT;
      }
      case 4: {
        return this.goingLeft ? Assets.PLAYER_4_LEFT : Assets.PLAYER_4_RIGHT;
      }
      default: {
        return "";
      }
    }
  }
  
  public String getFront() {
    switch(this.playerNumber) {
      case 1: {
        return Assets.PLAYER_1_FRONT;
      }
      case 2: {
        return Assets.PLAYER_2_FRONT;
      }
      case 3: {
        return Assets.PLAYER_3_FRONT;
      }
      case 4: {
        return Assets.PLAYER_4_FRONT;
      }
      default: {
        return "";
      }
    }
  }


  /*
  * ACCESSORS
  */

  void setPlayerNumber(int playerNumber) {
    this.playerNumber = playerNumber;
  }
  
  void setPosition(PVector p) {
    setX(p.x);
    setY(p.y);
  }
  
  void setX(float x) {
    this.x = x;
  }
  
  void setY(float y) {
    this.y = y;
  }

  void addKey() {
    if (this.keys < 9) {
      this.keys += 1;
    }
  }

  void setOffsetActive(boolean b) {
    this.offsetActive = b;
  }

  void setSize(float size) {
    this.size = size;
  }
}
