/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class PlayerSelectionActor {
  private static final float DEFAULT_TOUCH_COOLDOWN = .15f;

  public static final int WIDTH = 142;
  public static final int HEIGHT = 240;

  private static final float X_NEXT = .1f;
  private static final float Y_NEXT = .5f;
  private static final float X_PREVIOUS = .9f;
  private static final float Y_PREVIOUS = .5f;
  private static final float X_LOCK = .9f;
  private static final float Y_LOCK = .1f;

  private int selected;
  private ArrayList<Player> players;
  private boolean unlocked;
  private boolean lockable;
  private boolean validated;

  private float touchCooldown;

  private PImage characterImage;

  public float x;
  public float y;

  public PlayerSelectionActor(float x, float y, ArrayList<Player> players, boolean unlocked, String imgSource) {
    this.players = players;
    this.unlocked = unlocked;
    this.lockable = !unlocked;
    this.validated = false;
    this.selected = 0;
    this.selectPlayer();
    this.touchCooldown = 0;

    this.characterImage = loadImage(imgSource);

    this.x = x;
    this.y = y;
  }

  public void handleTouch(float x, float y) {
    if (this.touchCooldown == 0) {
      if (this.unlocked && this.lockable && this.touchesLockButton(x, y)) {
        this.unlocked = false;
        this.touchCooldown = DEFAULT_TOUCH_COOLDOWN * 8;
        return;
      }

      if (this.unlocked && this.touchesNext(x, y)) {
        this.next();
        this.touchCooldown = DEFAULT_TOUCH_COOLDOWN;
        return;
      }

      if (this.unlocked && this.touchesPrevious(x, y)) {
        this.previous();
        this.touchCooldown = DEFAULT_TOUCH_COOLDOWN;
        return;
      }

      if (!this.unlocked && this.touchesUnlock(x, y)) {
        this.unlocked = true;
        this.touchCooldown = DEFAULT_TOUCH_COOLDOWN;
        return;
      }
    }
  }

  public void draw() {
    drawBackground();
    drawPlayerList();
    drawLockButton();
    drawCharacter();
  }

  private void drawBackground() {
    fill(0);
    stroke(255);
    rect(this.x, this.y, WIDTH, HEIGHT);
  }

  private void drawPlayerList() {
    if (this.unlocked) {
      fill(255);
      textSize(20);
      textAlign(CENTER);
      text(this.selectPlayer().name, this.x + WIDTH / 2, this.y + HEIGHT * .5f);
      textSize(10);
      text(this.getPreviousPlayer().name, this.x + WIDTH / 2, this.y + HEIGHT * .2f);
      text(this.getNextPlayer().name, this.x + WIDTH / 2, this.y + HEIGHT * .8f);
    } else {
      fill(255);
      textSize(15);
      textAlign(CENTER);
      text("TOUCH TO\nUNLOCK", this.x + WIDTH / 2, this.y + HEIGHT * .5f);
    }
  }

  private void drawLockButton() {
    if (this.unlocked && this.lockable) {
      fill(255);
      textSize(20);

      text("X", this.x + X_LOCK * WIDTH, this.y + Y_LOCK * HEIGHT);
    }
  }

  private void drawCharacter() {
    if (this.unlocked) {
      image(this.characterImage, this.x + WIDTH / 2 - 64 / 2, this.y - 40);
    }
  }

  private boolean touchesNext(float x, float y) {
    if (x > this.x && x < this.x + WIDTH &&
        y > this.y + HEIGHT * .6f && y < this.y + HEIGHT) {
      return true;
    }

    return false;
  }

  private boolean touchesPrevious(float x, float y) {
    if (x > this.x && x < this.x + WIDTH &&
        y > this.y && y < this.y + HEIGHT * .4f) {
      return true;
    }

    return false;
  }

  private boolean touchesUnlock(float x, float y) {
    if (x > this.x && x < this.x + WIDTH &&
        y > this.y && y < this.y + HEIGHT) {
      return true;
    }

    return false;
  }

  private boolean touchesLockButton(float x, float y) {
    if (x > this.x + WIDTH * X_LOCK - 10 && x < this.x + WIDTH &&
        y > this.y && y + Y_LOCK - 10 < this.y + HEIGHT) {
      return true;
    }

    return false;
  }

  public Player selectPlayer() {
    return this.players.get(this.selected);
  }

  public boolean isUnlocked() {
    return this.unlocked;
  }

  public void coolTouchDown(float delta) {
    if (this.touchCooldown > 0f) {
      this.touchCooldown -= delta;
    } else {
      this.touchCooldown = 0f;
    }
  }

  private Player getNextPlayer() {
    if (this.selected >= this.players.size() - 1) {
      return this.players.get(0);
    }

    return this.players.get(this.selected + 1);
  }

  private Player getPreviousPlayer() {
    if (this.selected <= 0) {
      return this.players.get(this.players.size() - 1);
    }

    return this.players.get(this.selected - 1);
  }

  private void next() {
    this.selected++;

    if (this.selected >= this.players.size()) {
      this.selected = 0;
    }
  }

  private void previous() {
    this.selected--;

    if (this.selected < 0) {
      this.selected = this.players.size() - 1;
    }
  }
}
