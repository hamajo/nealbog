/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class MiniGameState implements IState {
  private GameController gameController;
  private MiniGame miniGame;
  private BoardState boardState;
  
  public MiniGameState(GameController gameController, BoardState boardState) {
    this.gameController = gameController;
    this.miniGame = gameController.getRandomMiniGame();
    this.miniGame.registerGameController(this.gameController);
    this.boardState = boardState;
  }
  
  public void init() {
    this.miniGame.init();
  }
  
  public void handleInput(InputHandler inputHandler) {
    this.miniGame.handleInput(inputHandler);
  }
  
  public IState update(float delta) {
    IState newState = this.miniGame.update(delta);
    if (newState.equals(this.miniGame)) {
      return this;
    } else {
      return this.boardState;
    }
  }
  
  public void draw() {
    this.miniGame.draw();
  }
}
