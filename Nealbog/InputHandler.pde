/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import vialab.SMT.event.*;
import vialab.SMT.util.*;
import vialab.SMT.*;
import vialab.SMT.swipekeyboard.*;
import vialab.SMT.renderer.*;

/*
 * The InputHandler object that will manage the calibration and the calibrated touches.
 */
public class InputHandler {
  /*
   * The calibration object.
   */
  private Calibration calibration;
  
  /*
   * The touches calibrated.
   */
  private ArrayList<Touch> touches;
  
  /*
   * Ctor for HamajoTouch.
   */
  public InputHandler(int windowWidth, int windowHeight) { 
    touches = new ArrayList<Touch>();
    calibration = new Calibration(windowWidth, windowHeight);
  }
  
  /*
   * Touches calibrated. An array is returned so that it can be used the same way has SMT.getTouches().
   * @returns Touch[] the calibrated touches.
   */
  public Touch[] getTouches() {
    Touch[] aTouches = new Touch[touches.size()];
    for (int i = 0; i < touches.size(); i++) {
      aTouches[i] = touches.get(i);
    }
    
    return aTouches;
  }

  /*
   * This method must be called in the main draw method, before everything.
   * This will update current SMT.Touch with the calibrated data.
   * Nothing will be done if the calibration is not over.
   */
  public void update() {
    vialab.SMT.Touch[] smtTouches = SMT.getTouches();
    if (smtTouches.length == 0 && !touches.isEmpty()) {
      // 0 SMT.Touch on the screen, so emptying touches. 
      touches = new ArrayList<Touch>();
    }

    // If there is at least one touch.
    if (smtTouches.length > 0) {
      removeUnuseTouches();
      
      // Will update already existing touches, and instanciate new ones.
      for (vialab.SMT.Touch smtTouch : smtTouches) {
        boolean updated = false;
        for (int i = 0; i < touches.size(); i++) {
          Touch touch = touches.get(i); 
          if (touch.getID() == smtTouch.cursorID && 
                    touch.getSessionID() == smtTouch.getSessionID()) {
            // This SMT.Touch is already existing in touches, so updating its position.
            touch.setX(smtTouch.x);
            touch.setY(smtTouch.y);
            updated = true;
          } else if (touch.getID() == smtTouch.cursorID &&
                    touch.getSessionID() != smtTouch.getSessionID()) {
            // Same ID found, but not the same sessionID. Removing this touch to instanciate a new one after.
            touches.remove(i);
          }
        }
        
        if (!updated) {
          // The touch has not been updated so instancing a new one.
          PVector v = calibration.getFixingVector(smtTouch.x, smtTouch.y);
          touches.add(new Touch(smtTouch, v));
        }
      }
    }
  }
  
  /*
   * This method will removed all touches that does not exist anymore.
   */
  private void removeUnuseTouches() {
    for (int i = 0; i < touches.size(); i++) {
      boolean stillExisting = false;
      for (vialab.SMT.Touch smtTouch : SMT.getTouches()) {
        if (smtTouch.getSessionID() == touches.get(i).getSessionID()) {
          stillExisting = true;
          break;
        }
      }
      
      if (!stillExisting) {
          touches.remove(i);
        }
    }
  }
  
  /*
   * @returns Calibration the calibration object.
   */
  public Calibration getCalibration() {
    return calibration;
  }
}
