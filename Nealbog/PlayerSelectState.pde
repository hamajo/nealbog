/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class PlayerSelectState implements IState {
  private GameController gameController;
  
  private PlayerSelectionActor player1;
  private PlayerSelectionActor player2;
  private PlayerSelectionActor player3;
  private PlayerSelectionActor player4;

  private boolean startGame;

  private PImage backgroundImg;
  private PFont font;

  PlayerSelectState(GameController gameController) {
    this.gameController = gameController;
    this.backgroundImg = loadImage(Assets.SELECT_PLAYER_BACKGROUND);

    this.font = createFont(Assets.PIRATE_FONT, 30);

    this.init();
  }
  
  void init() {
    this.gameController.resetGameController();
    this.player1 = new PlayerSelectionActor(
      width * .11f, height * .6f,
      gameController.getAPIPlayers(),
      true,
      Assets.PLAYER_1_FRONT
    );

    this.player2 = new PlayerSelectionActor(
      width * .33f,
      height * .6f,
      gameController.getAPIPlayers(),
      true,
      Assets.PLAYER_2_FRONT
    );

    this.player3 = new PlayerSelectionActor(
      width * .55f,
      height * .6f,
      gameController.getAPIPlayers(),
      false,
      Assets.PLAYER_3_FRONT
    );

    this.player4 = new PlayerSelectionActor(
      width * .77f,
      height * .6f,
      gameController.getAPIPlayers(),
      false,
      Assets.PLAYER_4_FRONT
    );


    this.startGame = false;
  }
  
  void handleInput(InputHandler inputHandler) {
    if (inputHandler.getTouches().length == 0) return;

    Touch touch = inputHandler.getTouches()[0];

    if (this.touchesStartButton(touch.getX(), touch.getY())) {
      boolean canStart = true;

      canStart = this.gameController.selectPlayer(this.player1.selectPlayer().id);
      canStart = this.gameController.selectPlayer(this.player2.selectPlayer().id);

      if (this.player3.isUnlocked()) {
        canStart = this.gameController.selectPlayer(this.player3.selectPlayer().id);
      }

      if (this.player4.isUnlocked()) {
        canStart = this.gameController.selectPlayer(this.player4.selectPlayer().id);
      }

      this.startGame = canStart;
    }

    player1.handleTouch(touch.getX(), touch.getY());
    player2.handleTouch(touch.getX(), touch.getY());
    player3.handleTouch(touch.getX(), touch.getY());
    player4.handleTouch(touch.getX(), touch.getY());
  }

  private boolean touchesStartButton(float x, float y) {
    if (x > width * .2f && x < width * .8f &&
        y > height * .4f && y < height * .6f) {
      return true;
    }

    return false;
  }
  
  IState update(float delta) {
    if (startGame) {
      this.gameController.registerPlayersParticipation();
      BoardState board = new BoardState(this.gameController);
      board.initializePlayerPosition();
      return board;
    }

    player1.coolTouchDown(delta);
    player2.coolTouchDown(delta);
    player3.coolTouchDown(delta);
    player4.coolTouchDown(delta);

    return this;
  }
  
  void draw() {
    image(backgroundImg, 0, 0, width, height);

    fill(0);
    textFont(this.font, 58);
    text("Le tresor du Capitaine Nealbog", width / 2, height * .15f);

    fill(255);
    textFont(this.font, 55);
    textAlign(CENTER);
    text("Lancer la partie", width / 2, height * .5f);

    player1.draw();
    player2.draw();
    player3.draw();
    player4.draw();
  }
}
