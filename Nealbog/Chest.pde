/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Chest {
  private boolean toDraw;
  private boolean open;
  private boolean found;

  private int nbKeysNeeded;
  private int square;

  private float x;
  private float y;
  private float counter;
  
  Chest(float x, float y, int square) {
    this.x = x;
    this.y = y;
    this.toDraw = false;
    this.nbKeysNeeded = 5;
    this.open = false;
    this.found = false;
    this.counter = .0f;

    this.square = square;
  }
  
  void update(float delta) {
    if (this.toDraw) {
      this.counter += delta;      
    }

    if (this.counter > 2f) {
      this.counter = .0f;
      this.toDraw = false;
    }
  }
  
  void draw() {
    if (this.found) {
      PImage chest;
      chest = loadImage(Assets.TREASURE_CLOSE_IMG);
      image(chest, x - 16, y - 15, chest.width * .05f, chest.height * .05f);
    }
    
    if (this.toDraw) {
      PImage chest;
      String text;
      if (this.open) {
        chest = loadImage(Assets.TREASURE_OPENED_IMG);
        text = "Felicitation !!!";
      } else {
        chest = loadImage(Assets.TREASURE_CLOSE_IMG);
        text = "Pas assez de cle pour ouvrir le coffre.";
      }
      
      textSize(32);
      textAlign(CENTER);
      text(text, width / 2, height * .15f);
      
      imageMode(CENTER);
      image(chest, width / 2, height / 2);
      imageMode(CORNER);
    }
  }
  
  void openChest(Player player) {
    this.found = true;
    this.toDraw = true;
    if (player.keys >= this.nbKeysNeeded) {
      this.open = true;
    }
  }
  
  boolean isChestOpened() {
    return this.open;
  }

  int getSquare() {
    return this.square;
  }
}
