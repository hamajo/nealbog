/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import http.requests.*;

class NetworkHandler {
  private String accessToken;

  NetworkHandler() {
    XML root = loadXML("config.xml");
    this.accessToken = root.getChild("token").getContent();
  }

  JSONObject getJsonData(String uri) {
    GetRequest get = new GetRequest(uri);
    get.addHeader("Authorization", "Bearer " + this.accessToken);
    get.send();

    String data = get.getContent();

    if (data == null || data.trim().equals("")) {
      return null;
    } else {
      PrintWriter pw = createWriter("tmp.json");
      pw.println(data);
      pw.flush();
      pw.close();
      
      return loadJSONObject("tmp.json");
    }
  }
  
  JSONObject postData(String uri) {
    PostRequest post = new PostRequest(uri);
    post.addHeader("Authorization", "Bearer " + this.accessToken);
    post.send();
    
    String data = post.getContent();
    if (data == null || data.trim().equals("")) {
      return null;
    } else {
      PrintWriter pw = createWriter("tmp.json");
      pw.println(data);
      pw.flush();
      pw.close();
      
      return loadJSONObject("tmp.json");
    }
  }
}
