/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

abstract class MiniGame implements IState {
  private GameController gameController;
  
  MiniGame() {}

  public final IState keepOn() {
    return this;
  }
  
  public final IState win() {
    this.addKeyToCurrentPlayer();
    return new BoardState(this.gameController);
  }
  
  public final IState lose() {
    return new BoardState(this.gameController);
  }
  
  void registerGameController(GameController gameController) {
    this.gameController = gameController;
  }
  
  private void addKeyToCurrentPlayer() {
    if (this.gameController != null) {
      this.gameController.addKeyToCurrentPlayer();
    }
  }
}
