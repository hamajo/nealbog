/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Game {
  private IState currentState;
  private IState nextState;
  private GameController gameController;
  private boolean started;
  
  private int currTime, prevTime;
  private float delta; 
  private boolean calibrate;

  Game() {
    this.started = false;
    this.gameController = new GameController();

    // import config.xml
    XML root = loadXML("config.xml");
    calibrate = root.getChild("calibrate").getContent().equals("1");
  }
  
  void start() {
    if (calibrate) {
      this.currentState = new CalibrationState(this.gameController);
    } else {
      this.currentState = new PlayerSelectState(this.gameController);
    }
    this.nextState = this.currentState;
    this.currentState.init();
    
    this.currTime = this.prevTime = millis();
    
    this.started = true;
  }

  void handleInput() {
    if (this.started) {
      this.gameController.getInputHandler().update();
      this.currentState.handleInput(this.gameController.getInputHandler());
    }
  }

  void update() {    
    if (this.started) {
      this.currTime = millis();
      this.delta = (this.currTime - this.prevTime) / 1000.0;
      this.prevTime = this.currTime;
      
      this.nextState = this.currentState.update(this.delta);
    }
  }

  void draw() {
    if (this.started) {
      this.currentState.draw();
      this.endLoop();
    }
  }
  
  public void registerMiniGame(MiniGame miniGame) {
    this.gameController.registerMiniGame(miniGame);
  }

  private void endLoop() {
    if (!this.currentState.equals(this.nextState)) {
      this.nextState.init();
      this.currentState = this.nextState;
    }
  }
}
