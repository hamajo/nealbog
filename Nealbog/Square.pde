/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Square {
  private float x;
  private float y;
  private float counter;

  private boolean hasMiniGame;

  private int size;

  private PImage img;
 
  public Square(float x, float y) {
    this.x = x;
    this.y = y;
    this.size = 30;
    this.counter = .0f;
    
    switch ((int) random(0, 2)) {
      case 0: {
        this.hasMiniGame = true;
        break;
      }
      case 1: {
        this.hasMiniGame = false;
        break;
      }
      default: {
        this.hasMiniGame = false;
        break;
      }
    }
    if (this.hasMiniGame) {
      this.img = loadImage(Assets.SQUARE_MASK);  
    } else {
      this.img = loadImage(Assets.SQUARE_IMG);  
    }
    
    
  }
  
  public boolean hasMiniGame() {
    return this.hasMiniGame;
  }
  
  public float getX() {
    return this.x;
  }
  
  public float getY() {
    return this.y;
  }
  
  public void update(float delta) {
  }
  
  public void draw() {
    imageMode(CENTER);
    image(this.img, x, y, this.img.width * .3f, this.img.height * .3f);
    imageMode(CORNER);
  }
  
  public PVector getVector() {
    return new PVector(this.x, this.y); 
  }
  
  public boolean isOver(PVector p) {
    if (p.x > this.x - this.size && p.x < this.x + this.size &&
          p.y > this.y - this.size && p.y < this.y + this.size) {
      return true;
    } 
    return false;
  }
}
