/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class BoardState implements IState {
  private Square[] squares;
  private GameController gameController;
  
  private float counter;
  private Die die;
  private boolean dieToRoll;
  
  private int boardStage;
  private int currPos;
  private int nbMove;
  
  private final float xOffset = - width * .022f;
  private final float yOffset = - height * .07f;
  
  private PImage background;
  private PImage keys;
  
  private Chest chest;

  private boolean touchLock;
  
  public BoardState(GameController gameController) {
    this.gameController = gameController;

    Square[] s = {
      new Square(width * .87f, height * .70f), new Square(width * .85f, height * .60f),  
      new Square(width * .77f, height * .54f), new Square(width * .69f, height * .48f),
      new Square(width * .60f, height * .37f), new Square(width * .51f, height * .32f),
      new Square(width * .45f, height * .39f), new Square(width * .40f, height * .50f),
      new Square(width * .35f, height * .60f), new Square(width * .30f, height * .51f),
      new Square(width * .31f, height * .42f), new Square(width * .33f, height * .28f),
      new Square(width * .38f, height * .22f), new Square(width * .35f, height * .12f),
      new Square(width * .28f, height * .08f), new Square(width * .20f, height * .10f),
      new Square(width * .13f, height * .08f), new Square(width * .09f, height * .15f),
      new Square(width * .09f, height * .27f), new Square(width * .15f, height * .29f),
      new Square(width * .20f, height * .40f), new Square(width * .17f, height * .46f),
      new Square(width * .11f, height * .43f), new Square(width * .07f, height * .50f),
      new Square(width * .07f, height * .60f), new Square(width * .09f, height * .70f),
      new Square(width * .19f, height * .75f), new Square(width * .20f, height * .88f),
      new Square(width * .28f, height * .92f), new Square(width * .33f, height * .85f),
      new Square(width * .40f, height * .80f), new Square(width * .45f, height * .89f),
      new Square(width * .53f, height * .93f), new Square(width * .62f, height * .85f),
      new Square(width * .70f, height * .85f), new Square(width * .79f, height * .77f)
    };
    
    this.squares = s;
    this.counter = .0f;
    this.die = new Die(width * .06f, height * .9f);
    this.boardStage = 0;
    this.currPos = 0;
    this.nbMove = 0;
    
    this.background = loadImage(Assets.BOARD_BACKGROUND);
    this.keys = loadImage(Assets.KEY_IMG);
    initializeChest();
  }
  
  void init() {
  }
  
  void handleInput(InputHandler inputHandler) {
    if (inputHandler.getTouches().length == 0) {
      this.touchLock = false;
      return;
    }
    Touch touch = inputHandler.getTouches()[0];
    
    if (this.die.isOver(new PVector(touch.getX(), touch.getY())) 
          && this.boardStage == 0 && !this.touchLock) {
      this.dieToRoll = true;
      this.touchLock = true;
      return;
    }
  }
  
  IState update(float delta) {
    this.die.update(delta);
    this.counter += delta;
    if (this.counter > 3f && this.chest.isChestOpened()) {
      return new GameOverState(this.gameController);
    } else if (this.chest.isChestOpened()) {
      // Doing nothing on the board, just waiting a little bit.
      return this;
    }

    this.chest.update(delta);
    for (Square square : this.squares) {
      square.update(delta);
    }
    
    if (this.die.isLocked() && this.boardStage == 0) {
      this.gameController.nextPlayer();
      for (Player player : this.gameController.getPlayers()) {
        player.isCurrentPlayer = false;
      }
      this.gameController.getCurrentPlayer().isCurrentPlayer = true;
      this.die.unlock();
    }
    
    if (this.dieToRoll && this.counter > .2f) {
      this.die.roll();
      this.dieToRoll = false;
      this.counter = .0f;
      this.nextStage();
    }
    
    if (this.boardStage == 1 && this.die.endOfRoll()) {
      // If we are in the moving stage.
      if (this.nbMove == 0) {
        this.nbMove = this.die.getValue();
      } 

      if (this.counter > .4f) {
        int nextPos = (getCurrentPlayerSquare() + 1) % this.squares.length;

        if (this.squares[getCurrentPlayerSquare()].getX() < this.squares[nextPos].getX()) {
          this.gameController.getCurrentPlayer().goingLeft = false;
        } else {
          this.gameController.getCurrentPlayer().goingLeft = true;
        }

        this.setPlayerToSquare(this.gameController.getCurrentPlayer(), this.squares[nextPos]);
        if (nextPos == chest.square && chest.found && 
              this.gameController.getCurrentPlayer().keys >= chest.nbKeysNeeded) {
          this.nbMove = 1;
        }
        this.activeOffset();
        this.counter = .0f;
        this.nbMove--;

        if (this.nbMove == 0) {
          if (this.getCurrentPlayerSquare() == this.chest.getSquare()) {
            // Player is on the chest square.
            this.chest.openChest(this.gameController.getCurrentPlayer());
          }

          if (this.squares[getCurrentPlayerSquare()].hasMiniGame() && !this.chest.isChestOpened()) {
            // Player is on a mini-game square, and the chest has not been opened.
            this.nextStage();
            return new MiniGameState(this.gameController, this);
          }

          this.nextStage();
        } 
      }
    }
    
    for (Player player : this.gameController.getPlayers()) {
      player.update(delta);
    }
    return this;
  }
  
  void draw() {
    image(this.background, 0, 0, width, height);
    for (Square square : this.squares) {
      square.draw();
    }
    this.die.draw();
    
    for (int i = 0; i < this.gameController.getPlayers().size(); i++) {
      this.gameController.getPlayers().get(i).draw();
      PImage img = loadImage(this.gameController.getPlayers().get(i).getFront());

      image(img, ((i + 1) * 125f) + width * .5f, 
            height * .02f, img.width * .6f, img.height * .6f);

      textMode(CENTER);
      textSize(32);

      text("" + this.gameController.getPlayers().get(i).keys, 
            ((i + 1) * 127f) + width * .53f, height * .065f);
      image(keys, ((i + 1) * 125f) + width * .54f, height * .02f, 
            img.width * .6f, img.height * .6f);
      textSize(10);
      text(this.gameController.getPlayers().get(i).name, 
            ((i + 1) * 127f) + width * .53f, height * .09f);
      if (this.gameController.getPlayers().get(i).isCurrentPlayer) {
        fill(255, 0, 0);
        triangle(((i + 1) * 127f) + width * .53f, height * .1f, 
                 ((i + 1) * 127f) + width * .52f, height * .13f,
                 ((i + 1) * 127f) + width * .54f, height * .13f);
        fill(0, 0, 0);
      }
    }
    
    this.chest.draw();
  }
  
  void nextStage() {
    this.boardStage = this.boardStage == 1 ? 0 : this.boardStage + 1;
  }
  
  int getCurrentPlayerSquare() {
    for (int i = 0; i < this.squares.length; i++) {
      if (this.squares[i].getX() + xOffset == this.gameController.getCurrentPlayer().x && 
           this.squares[i].getY() +yOffset == this.gameController.getCurrentPlayer().y) {
        return i;
      }
    }
    return 0;
  }
  
  void setPlayerToSquare(Player p, Square s){
    p.setX(s.getX() + this.xOffset);
    p.setY(s.getY() + this.yOffset);
  }
  
  void activeOffset() {
    for (Player p : this.gameController.getPlayers()) {
      p.offsetActive = false;
    }
    
    for (Player p : this.gameController.getPlayers()) {
      for (Player j : this.gameController.getPlayers()) {
        if (p.x == j.x && p.y == j.y && p.id != j.id) {
          p.offsetActive = true;
        }
      }

    }
  }
  
  void initializeChest() {
    int n = (int) random(0, this.squares.length);
    this.chest = new Chest(this.squares[n].getX(), this.squares[n].getY(), n);
  }

  void initializePlayerPosition() {
    for (Player player : this.gameController.getPlayers()){
      this.setPlayerToSquare(player, this.squares[0]);
      player.setSize(.9f);
      player.setOffsetActive(true);
    }
    this.gameController.getPlayers().get(0).isCurrentPlayer = true;
  }
}
