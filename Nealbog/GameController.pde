/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class GameController {
  private ArrayList<MiniGame> miniGames;
  private InputHandler inputHandler;
  private NetworkHandler networkHandler;
  private ArrayList<Player> players;
  private ArrayList<Player> apiPlayers;
  private int currentPlayerIndex;
  
  private int applicationId;
  private String gameListApiUrl;
  private String playerListApiUrl;
  
  GameController() {
    this.miniGames = new ArrayList<MiniGame>();
    this.inputHandler = new InputHandler(width, height);
    this.networkHandler = new NetworkHandler();
    this.players = new ArrayList<Player>();
    this.apiPlayers = new ArrayList<Player>();
    this.currentPlayerIndex = 0;

    // import config.xml
    XML root = loadXML("config.xml");
    String apiUri = root.getChild("api_url").getContent();
    this.applicationId = root.getChild("application_id").getIntContent();

    // retrieve API description from the scoreboard API root
    JSONObject apiDescription = this.networkHandler.getJsonData(apiUri);
    this.gameListApiUrl = apiDescription.getString("games");
    this.playerListApiUrl = apiDescription.getString("players");
    
    // retrieve the list of players registered in the Scoreboard web service
    initApiPlayerList();
  }
  
  void registerMiniGame(MiniGame miniGame) {
    this.miniGames.add(miniGame);
    println("Registered new mini game: " + miniGame.getClass().getName());
  }
  
  MiniGame getRandomMiniGame() {
    if (this.miniGames.size() != 0) {
      int index = int(random(this.miniGames.size()));
      MiniGame miniGame = this.miniGames.get(index);
      miniGame.registerGameController(this);
      
      return miniGame;
    } else {
      println("No mini game has been registered!");
      System.exit(0);
      return null;
    }
  }
  
  boolean selectPlayer(int id) {
    for (Player player : this.apiPlayers) {
      if (player.id == id) {
        if (this.players.indexOf(player) != -1) {
          return false;
        }
        
        this.players.add(player);
        return true;
      }
    }
    
    return false;
  }
  
  Player getCurrentPlayer() {
    return this.players.get(this.currentPlayerIndex);
  }
  
  void nextPlayer() {
    this.currentPlayerIndex++;
    if (this.currentPlayerIndex >= this.players.size()) {
      this.currentPlayerIndex = 0;
    }
  }
  
  private void registerPlayerParticipation(Player player) {
    String url = this.gameListApiUrl;
    url += str(this.applicationId) + "/";
    url += "play" + "/";
    url += str(player.id) + "/";
    
    JSONObject result = this.networkHandler.postData(url);
  }
  
  void registerPlayerWin(Player player) {
    String url = this.gameListApiUrl;
    url += str(this.applicationId) + "/";
    url += "win" + "/";
    url += str(player.id) + "/";
    
    JSONObject result = this.networkHandler.postData(url);
  }
  
  void registerPlayersParticipation() {
    for (int i = 0; i < this.players.size(); i++) {
      this.registerPlayerParticipation(this.players.get(i));
      this.players.get(i).setPlayerNumber(i + 1);
    }
  }
  
  void addKeyToCurrentPlayer(){
    this.getCurrentPlayer().addKey();
  }

  private void initApiPlayerList() {
    if (this.playerListApiUrl != null) {
      JSONObject data = this.networkHandler.getJsonData(this.playerListApiUrl);
      JSONArray results = data.getJSONArray("results");
      
      this.apiPlayers = new ArrayList<Player>();
      for (int i = 0; i < results.size(); i++) {
        JSONObject player = results.getJSONObject(i);
        this.apiPlayers.add(new Player(player.getInt("id"), player.getString("name")));
      }
    }
  }
  
  InputHandler getInputHandler() {
    return inputHandler;
  }

  ArrayList<Player> getPlayers() {
    return players;
  }

  public ArrayList<Player> getAPIPlayers() {
    return this.apiPlayers;
  }

  public void resetGameController() {
    for (Player p : players) {
      p.keys = 0;
    }
    players = new ArrayList<Player>();
    currentPlayerIndex = 0;
  }
}

