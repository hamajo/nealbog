/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class MiniGameTest extends MiniGame {
  private boolean win;
  private boolean lose;

  public MiniGameTest() {
    this.win = false;
    this.lose = false;
  }

  public void init() {
      this.win = false;
      this.lose = false;
  }

  public void handleInput(InputHandler inputHandler) {
      if (inputHandler.getTouches().length == 0) return;

      Touch touch = inputHandler.getTouches()[0];

      if (this.touchesWin(touch.getX(), touch.getY())) {
        this.win = true;
        return;
      }

      if (this.touchesLose(touch.getX(), touch.getY())) {
        this.lose = true;
        return;
      }
  }

  public IState update(float delta) {
    if (this.win) {
      return win();
    }

    if (this.lose) {
      return lose();
    }

    return keepOn();
  }

  public void draw() {
    background(0);
    textSize(80);
    textAlign(CENTER);
    fill(255);

    text("WIN", width * .25f, height * .5f);
    text("LOSE", width * .75f, height * .5f);
  }

  private boolean touchesWin(float x, float y) {
    if (x < width * .5f) {
      return true;
    }

    return false;
  }

  private boolean touchesLose(float x, float y) {
    if (x > width * .5f) {
      return true;
    }

    return false;
  }
}
