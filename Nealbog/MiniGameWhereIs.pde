/*
Le trésor du Capitaine Neal Bog - a multiplayer board game for multi-touch TUIO tables
    Copyright (C) 2016 Hamajo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class MiniGameWhereIs extends MiniGame {
  private boolean win;
  private boolean lose;
  private boolean startGame;
  private boolean touchLock;

  private float timeLimit;
  private float gameOver;
  private float size;

  private PImage pirate1;
  private PImage pirate2;
  private PImage pirate3;
  private PImage pirate4;
  private PImage background;

  private ArrayList<WhereIsPirate> pirates;

  private float countdown;
  private int toFind;


  public MiniGameWhereIs() {
    this.win = false;
    this.lose = false;
    this.timeLimit = 5000f;
    this.startGame = false;
    this.size = 100;
    this.touchLock = false;
    this.countdown = .0f;
    this.pirates = new ArrayList<WhereIsPirate>();
  }

  public void init() {
      this.win = false;
      this.lose = false;
      this.startGame = false;
      this.timeLimit = 5000f;

      pirate1 = loadImage(Assets.PLAYER_1_FRONT);
      pirate2 = loadImage(Assets.PLAYER_2_FRONT);
      pirate3 = loadImage(Assets.PLAYER_3_FRONT);
      pirate4 = loadImage(Assets.PLAYER_4_FRONT);
      background = loadImage(Assets.BOARD_BACKGROUND);

      this.pirates = new ArrayList<WhereIsPirate>();
      while (this.pirates.size() < 200) {
        this.pirates.add(new WhereIsPirate(
            random(100, width - 100),
            random(100, height - 100),
            (int) random(0, 3)
            )
        );
      }

      toFind = (int) random(0, this.pirates.size());
      this.pirates.get(toFind).colour = 4;
  }

  public void handleInput(InputHandler inputHandler) {
      if (inputHandler.getTouches().length == 0) {
        touchLock = false;
        return;
      }

      if (!startGame) {
        for (Touch t : inputHandler.getTouches()) {
          if (t.y > height / 4 && t.y < height * .75f) {
            gameOver = millis() + timeLimit;
            startGame = true;
            touchLock = true;
            return;
          }
        }
      } 

      if (startGame && !this.win && !this.lose) {
        for (Touch t : inputHandler.getTouches()) {
          if (t.y > pirates.get(toFind).y && t.y < pirates.get(toFind).y + pirate4.height &&
              t.x > pirates.get(toFind).x && t.x < pirates.get(toFind).x + pirate4.width) {
            win = true;
            countdown = .0f;
          }
        }
      }
  }

  public IState update(float delta) {
    countdown += delta;

    if (millis() > gameOver && startGame && !this.lose) {
      this.lose = true;
      countdown = .0f;
    }

    if (this.win && countdown > 3f) {
      return win();
    }

    if (this.lose && countdown > 3f) {
      return lose();
    }

    return keepOn();
  }

  public void draw() {
    image(background, 0, 0, width, height);
    textAlign(CENTER);
    fill(255, 255, 255);
    if (!startGame) {
      textSize(28);
      text("Trouvez le pirate gris dans le temps imparti !", width / 2, height / 4);

      textSize(42);
      text("START MINI GAME", width / 2, height / 2);
      return;
    }

    if (!this.win && !this.lose) {
      textSize(72);
      int timeLeft = (int) ((gameOver - millis()) / 1000);
      text("" + timeLeft, width * .95f, height * .1f);
      for (WhereIsPirate p : pirates){
        image(getPirateImg(p.colour), p.x, p.y);
      }
    }

    if (this.win) {
      textSize(88);
      text("VICTOIRE !", width / 2, height / 2);
      image(pirate4, pirates.get(toFind).x, pirates.get(toFind).y);
    } else if (this.lose) {
      textSize(88);
      text("DEFAITE !", width / 2, height / 2);
      image(pirate4, pirates.get(toFind).x, pirates.get(toFind).y);
    }
  }

  private PImage getPirateImg(int colour) {
    switch (colour) {
      case 0: {
        return pirate1;
      }
      case 1: {
        return pirate2;
      }
      case 2: {
        return pirate3;
      }
      case 4: {
        return pirate4;
      }
      default: {
        return pirate1;
      }
    }
  }
}
