# NEALBOG #

Nealbog est un jeu de plateau dans lequel 2 à 4 joueurs s'affrontent pour remporter le trésor du capitaine Neal Bog.
Remportez des clés en gagnant aux mini-jeux que vous rencontrerez sur l'île pour ouvrir le coffre au trésor!

## Installation ##

Nealbog est développé sous Processing pour table tactile fonctionnant à travers la technologie TUIO.

Pour installer le jeu:

* Installer processing 2.2.1
* Dans Processing, installer les librairies:
    * Simple Multi Touch (SMT)
    * HTTP Requests for Processing

```
#!bash

git clone https://bitbucket.org/hamajo/nealbog.git

# Copier le dossier httprequests_processing présent
# dans le dossier vers le dossier sketchbook/libraries
# créé lors de l'installation de Processing

cd nealbog/Nealbog
cp config.sample.xml config.xml
```

* Editer les éléments de configuration du fichier config.xml (*optionnel*):
    * **token**: Le token d'accès à l'API Scoreboard délivré pour votre application.
    * **api_url**: l'url de la racine de l'API Scoreboard.
    * **application_id**: l'id du jeu dans la BDD du WebService Scoreboard.

* Lancer Processing
* Ouvrir le dossier Nealbog
* Lancer le jeu